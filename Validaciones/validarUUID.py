import uuid

#se verifica si el codigo es del tipo UUID. por ejemplo: "b172a2ab-5900-4532-bd68-68a041752017"
def isValidUUID(codigo_UUID):
    try:
        uuid.UUID(str(codigo_UUID))
        return True
    except ValueError:
        return False