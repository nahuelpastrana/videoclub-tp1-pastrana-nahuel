from datetime import date, datetime

#Se verifica si la fecha(String) es de la forma "AAAA/MM/DD".
def fechaValida(fecha):
    if len(fecha)==10 and fecha[4]=="/" and fecha[7]=="/":
        return True
    else:
        return False


#Se devuelve un date con el año, mes y dia especificados en la variable fecha_str.
def nuevaFecha(fecha_str):
    return date(year = int(fecha_str[:4]), month=int(fecha_str[5:7]), day=int(fecha_str[8:10]))

#se devuelve un fecha con el formato "AAAA/MM/DD", siendo el inicial del tipo "AAAA-MM-DD"
def formatoValido(fecha_str):
    fecha_str = fecha_str[8:10] + fecha_str[4:8] + fecha_str[:4]
    fecha_str = datetime.strptime(fecha_str, "%d-%m-%Y").strftime('%Y/%m/%d') 
    return fecha_str   