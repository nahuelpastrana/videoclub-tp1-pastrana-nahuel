from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os	

PWD = os.path.abspath(os.curdir)	

app = Flask(__name__)    
app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///{}/BaseDeDatos/videoclub.db'.format(PWD).replace(chr(92), "/")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app) 

