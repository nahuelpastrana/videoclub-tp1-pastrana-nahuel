from flask import jsonify, Response
from Aplicacion.aplicacion import app, db
from Entidades.renta import Renta
import xmltodict        #esta libreria nos permite representar un archivo xml mediante un diccionario.
import xml.etree.ElementTree as ET      #esta libreria nos permite crear un archivo xml.
from Validaciones.fecha import *

@app.route("/rentas/", methods=["PUT"])
def new_renta():
    from flask import request
    from Validaciones.validarUUID import isValidUUID
    from Validaciones.status import estadoValido
    from xml.parsers.expat import ExpatError
    
    try:     
        xml_data = xmltodict.parse(request.data)
    except ExpatError:
        return("XML no valido"), 400    

    object_id = xml_data["rent"]["object_id"]
    if(not isValidUUID(object_id)):
        return ("El codigo UUID del objeto es invalido."), 400
    else:
        client_id = xml_data["rent"]["client_id"]
        if (not isValidUUID(client_id)):
            return ("El codigo UUID del cliente es invalido."), 400
        else:
            status = xml_data["rent"]["details"]["status"]
            if(not estadoValido(status)):
                return ("El estado del objeto no es valido."), 400
            else:
                until = xml_data["rent"]["details"]["until"]
                if(not fechaValida(until)):
                    return ("La fecha del objeto no es valida."), 400
                else:
                    nueva_renta = Renta()
                    codigo_estado = 0
                    if Renta.query.get(object_id) is None:
                        nueva_renta.object_id = object_id
                        nueva_renta.client_id = client_id
                        nueva_renta.status = status
                        nueva_renta.until = nuevaFecha(until)
                        codigo_estado = 201
                    else:
                        nueva_renta = Renta.query.get(object_id)
                        nueva_renta.client_id = client_id
                        nueva_renta.status = status
                        nueva_renta.until = nuevaFecha(until)  
                        codigo_estado = 204
                                            
                    db.session.add(nueva_renta)
                    db.session.commit()

                    return (""), codigo_estado

@app.route("/rentas/", methods=["GET"])
def data_rentas():
    rentas = Renta.query.order_by(Renta.object_id).all()
    elemento_rentas = ET.Element("rentas")

    for x in rentas:
        elemento_renta = ET.SubElement(elemento_rentas, "renta")
        ET.SubElement(elemento_renta, "object_id").text = str(x.object_id)
        ET.SubElement(elemento_renta, "client_id").text = str(x.client_id)
        elemento_details = ET.SubElement(elemento_renta, "details")
        ET.SubElement(elemento_details, "status").text = x.status
        ET.SubElement(elemento_details, "until").text = formatoValido(x.until.isoformat())

    xml = ET.tostring(elemento_rentas)
    return Response(xml, mimetype='text/xml'), 200  



@app.route("/rentas/<codigo_uuid>", methods=["DELETE"])
def delete_renta(codigo_uuid):
    if Renta.query.get(codigo_uuid) is None:
        return ("No existe la renta con el codigo "+str(codigo_uuid)+"."), 404
    else:
        renta = Renta.query.get(codigo_uuid)
        db.session.delete(renta)
        db.session.commit()
        
        return (""), 204

