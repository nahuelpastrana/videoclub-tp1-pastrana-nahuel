**Trabajo Practico Nº1 Videoclub**

Se preparará una API REST que se encarge de controlar si las peticiones de rentas de un videoclub son correctas, validando sus campos y que el tipo de informacion sea en XML.


**Pre-requisitos**
- Python 3.8.5
- Postman 7.34


**Instalación**

Una vez situados en el directorio del proyecto clonado, debemos instalar las librerias necesarias para llevar a cabo la API. Dichas librerias se encuentran en el requirements.txt, para instalarlas debemos hacer los siguintes pasos:

1. Creamos el entorno virtual en el directorio del proyecto, en mi caso C:\Users\Napo\Desktop\videoclub-tp1-pastrana-nahuel.

En Windows.

`> py -m venv C:\Users\Napo\Desktop\videoclub-tp1-pastrana-nahuel`

En Ubuntu

`$ python3 -m venv C:\Users\Napo\Desktop\videoclub-tp1-pastrana-nahuel`

2. Activamos el entorno, ingresando primero al directorio Scripts en Windows o bin en Ubuntu.

En Windows

`> Scripts\activate`

En Ubuntu

`$ . videoclub-tp1-pastrana-nahuel/bin/activate`

3. Finalmente, instalamos los paquetes del requirements.

`pip3 install -r requirements.txt`


**Ejecucion de servidor**

Una vez instalado todo lo necesario, tenemos que ejecutar el server mediante la siguiente instruccion:

En windows:    `> python.exe server.py`

En Ubuntu:    `$ python3 server.py`

De esta manera ya tenemos el servidor activado.


**Pruebas unitarias**

Para ejecutar los tests unitarios, debemos ir al directorio Test, y ejecutar el archivo test.py.

En windows:    `> python.exe test.py`

En Ubuntu:    `$ python3 test.py`


**Ejemplos de uso**

Para ver los ejemplos, tenemos que importar la coleccion EJEMPLOS DE USO.json en Postman. Dicha coleccion muestra como agregar, modificar, obtener y eliminar rentas. Tambien se prueba los casos donde el XML no es valido, como asi tambien los codigos UUID, estados y fecha.

