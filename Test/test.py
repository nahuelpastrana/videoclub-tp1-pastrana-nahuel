import unittest
import requests

class BaseTestClass(unittest.TestCase):
    #se definen las variables que usaremos en los tests
    def setUp(self):
        self.url_rentas = "http://127.0.0.1:4000/rentas/" 

    #verifica si la peticion del "PUT" de la renta se completo exitosamente, es decir, si el codigo de estado es 201    
    def test_1_add_new_rent(self):
        xml_renta = "<rent><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>\
            b172a2ab-6666-4532-bd68-688241072017</client_id><details><status>RETURN</status>\
                <until>2020/10/01</until></details></rent>"

        r = requests.put(self.url_rentas, xml_renta)
        self.assertEqual(r.status_code, 201)
        print ("TEST 1: PUT para agregar una nueva renta, codigo de estado: 201")

    #verifica si la peticion del "PUT" de la renta se completo exitosamente, es decir, si el codigo de estado es 204    
    def test_2_update_rent(self):
        xml_update_renta = "<rent><object_id>b172a2ab-5900-4532-bd68-68a041752017</object_id><client_id>\
            b172a2ab-6666-4532-bd68-688241072017</client_id><details><status>RENT</status>\
                <until>2020/10/13</until></details></rent>"

        r = requests.put(self.url_rentas, xml_update_renta)
        self.assertEqual(r.status_code, 204)
        print ("TEST 2: PUT para modificar una renta, codigo de estado: 204")

    #verifica si la peticion del "PUT" tuvo un error de validacion del formato del XML, es decir, si el codigo de estado es 400   
    def test_3_add_rent_with_invalid_xml_format(self):
        xml_renta_formato_xml_invalido = "<rent>670b9562-b30d-52d5-b827-655787665500</object_id><client_id>\
            4272a2ab-6666-4532-bd68-688241072017</client_id><details><status>DELIVERY_TO_RENT</status>\
                <until>2020/10/15</until></details></rent>"

        r = requests.put(self.url_rentas, xml_renta_formato_xml_invalido)
        self.assertEqual(r.status_code, 400)
        print ("TEST 3: PUT para agregar una renta con XML invalido, codigo de estado: 400")

    #verifica si la peticion del "PUT" tuvo un error de validacion con el objeto UUID, es decir, si el codigo de estado es 400   
    def test_4_add_rent_with_invalid_objectUUID_code(self):
        xml_renta_codigo_objectUUID_invalido = "<rent><object_id>a2ab-5900-4532-bd68-68a041752017</object_id><client_id>\
            4272a2ab-6666-4532-bd68-688241072017</client_id><details><status>DELIVERY_TO_RENT</status>\
                <until>2020/10/15</until></details></rent>"

        r = requests.put(self.url_rentas, xml_renta_codigo_objectUUID_invalido)
        self.assertEqual(r.status_code, 400)
        print ("TEST 4: PUT para agregar una renta con objeto UUID invalido, codigo de estado: 400")

    #verifica si la peticion del "PUT" tuvo un error de validacion con el cliente UUID, es decir, si el codigo de estado es 400   
    def test_5_add_rent_with_invalid_clientUUID_code(self):
        xml_renta_codigo_clientUUID_invalido = "<rent><object_id>670b9562-b30d-52d5-b827-655787665500</object_id><client_id>\
            4272a2ab-6-4532-bd68-688241072017</client_id><details><status>DELIVERY_TO_RENT</status>\
                <until>2020/10/15</until></details></rent>"

        r = requests.put(self.url_rentas, xml_renta_codigo_clientUUID_invalido)
        self.assertEqual(r.status_code, 400)
        print ("TEST 5: PUT para agregar una nueva renta con cliente UUID invalido, codigo de estado: 400")

    #verifica si la peticion del "PUT" tuvo un error de validacion del estado, es decir, si el codigo de estado es 400   
    def test_6_add_rent_with_invalid_status(self):
        xml_renta_estado_invalido = "<rent><object_id>670b9562-b30d-52d5-b827-655787665500</object_id><client_id>\
            4272a2ab-6666-4532-bd68-688241072017</client_id><details><status>RENTED</status>\
                <until>2020/10/15</until></details></rent>"

        r = requests.put(self.url_rentas, xml_renta_estado_invalido)
        self.assertEqual(r.status_code, 400)
        print ("TEST 6: PUT para agregar una nueva renta con estado invalido, codigo de estado: 400")

    #verifica si la peticion del "PUT" tuvo un error de validacion con la fecha, es decir, si el codigo de estado es 400   
    def test_7_add_rent_with_invalid_date(self):
        xml_renta_fecha_invalida = "<rent><object_id>670b9562-b30d-52d5-b827-655787665500</object_id><client_id>\
            4272a2ab-6666-4532-bd68-688241072017</client_id><details><status>DELIVERY_TO_RENT</status>\
                <until>2020-10-15</until></details></rent>"

        r = requests.put(self.url_rentas, xml_renta_fecha_invalida)
        self.assertEqual(r.status_code, 400)
        print ("TEST 7: PUT para agregar una nueva renta con fecha invalida, codigo de estado: 400")

    #verifica si la peticion del "GET" de la renta fue correcta, es decir, si el codigo de estado es 200
    def test_8_get_all_rents(self):
        r = requests.get(self.url_rentas)
        self.assertEqual(r.status_code, 200)
        print ("TEST 8: GET de las rentas, codigo de estado: 200")

    #verifica si la peticion del "DELETE" del cliente se completo exitosamente, es decir, si el codigo de estado es 204
    def test_9_delete_rent(self):
        codigo_object_uuid = "b172a2ab-5900-4532-bd68-68a041752017"
        r = requests.delete(self.url_rentas+codigo_object_uuid)
        self.assertEqual(r.status_code, 204)     
        print ("TEST 9: DELETE de una renta, codigo de estado: 204")

    #verifica si la peticion del "DELETE" no pudo completarse, es decir, si el codigo de estado es 404   
    def test__10_delete_non_existent_rent(self):
        codigo_object_uuid = "b172a2ab-5900-4532-bd68-68a041752017"
        r = requests.delete(self.url_rentas+codigo_object_uuid)
        self.assertEqual(r.status_code, 404)     
        print ("TEST 10: DELETE de una renta inexistente, codigo de estado: 404")

    
 
if __name__ == "__main__":
    unittest.main()